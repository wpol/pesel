public class ZadPesel {
    public static void main(String[] args) {
        String pesel = "66112202370";
        printPesel(pesel);

    }

    private static void printPesel(String pesel) {
        if(!isPesel(pesel)) {
            System.out.println("This is not a valid pesel");
            return;
        }
            sex(pesel);
            dateOfBirth(pesel);
            serialNumber(pesel);
    }

    private static void dateOfBirth(String pesel){

        int monthCase = Integer.parseInt(pesel.substring(2,4));
        String month = "";
        switch (monthCase){
            case 81: case 1: case 21: case 41: case 61:
                month = "styczen";
                break;
            case 82: case 2: case 22: case 42: case 62:
                month = "luty";
                break;
            case 83: case 3: case 23: case 43: case 63:
                month = "marzec";
                break;
            case 84: case 4: case 24: case 44: case 64:
                month = "kwiecien";
                break;
            case 85: case 5: case 25: case 45: case 65:
                month = "maj";
                break;
            case 86: case 6: case 26: case 46: case 66:
                month = "czerwiec";
            case 87: case 7: case 27: case 47: case 67:
                month = "lipiec";
                break;
            case 88: case 8: case 28: case 48: case 68:
                month = "sierpien";
                break;
            case 89: case 9: case 29: case 49: case 69:
                month = "wrzesien";
                break;
            case 90: case 10: case 30: case 50: case 70:
                month = "pazdziernik";
                break;
            case 91: case 11: case 31: case 51: case 71:
                month = "listopad";
                break;
            case 92: case 12: case 32: case 52: case 72:
                month = "grudzien";
                break;

        }

        String day = pesel.substring(4,6);
        String preYear = "";
        if (monthCase >= 1 && monthCase <= 22){
            preYear = "19";
        }else if (monthCase >= 21 && monthCase <= 32){
            preYear = "20";
        }else if (monthCase >= 41 && monthCase <= 52){
            preYear = "21";
        }else if (monthCase >= 61 && monthCase <= 72){
            preYear = "22";
        }else preYear = "18";

        String year = preYear + pesel.substring(0, 2);
        System.out.print("Date of birth: " + day + " " + month + " " + year + "\n");
    }
    private static void serialNumber(String pesel){
        System.out.println("Serial number: " + pesel.substring(6, 10));
}
    private static void sex(String pesel) {
        if(pesel.charAt(9) % 2 == 0) {
            System.out.println("kobieta");
        }else System.out.println("mezczyzna");
    }

    private static boolean isPesel(String pesel) {
        if (pesel.length() != 11) {
            return false;
        }
        for (int i = 0; i < 11; i++) {
            if (!Character.isDigit(pesel.charAt(i))) {
                return false;
            }
        }
        int sum = 0;
        int[] weight = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};
        for (int i = 0; i < pesel.length() ; i++) {
            int temp = weight[i] * (int)(pesel.charAt(i));
            sum += temp;
        }

        if (sum % 10 == 0) {
            return true;
        }
        return false;
    }
}
